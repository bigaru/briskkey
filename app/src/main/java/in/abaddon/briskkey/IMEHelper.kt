package `in`.abaddon.briskkey

import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.ExtractedTextRequest
import android.view.inputmethod.InputConnection

interface IMEHelper {
    val inputConnection: InputConnection
    val inputEditorInfo: EditorInfo

    fun performEnter(){
        when (inputEditorInfo.imeOptions and (EditorInfo.IME_MASK_ACTION or EditorInfo.IME_FLAG_NO_ENTER_ACTION)) {
            EditorInfo.IME_ACTION_GO -> inputConnection.performEditorAction(EditorInfo.IME_ACTION_GO)
            EditorInfo.IME_ACTION_NEXT -> inputConnection.performEditorAction(EditorInfo.IME_ACTION_NEXT)
            EditorInfo.IME_ACTION_SEARCH -> inputConnection.performEditorAction(EditorInfo.IME_ACTION_SEARCH)
            EditorInfo.IME_ACTION_SEND -> inputConnection.performEditorAction(EditorInfo.IME_ACTION_SEND)
            EditorInfo.IME_ACTION_DONE -> inputConnection.performEditorAction(EditorInfo.IME_ACTION_DONE)
            else -> sendKeyCode(KeyEvent.KEYCODE_ENTER)
        }
    }

    fun sendKeyCode(keyCode: Int) {
        inputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, keyCode))
        inputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_UP, keyCode))
    }

    fun sendKeyCode(keyCode: Int, metaCode: Int) {
        inputConnection.sendKeyEvent(KeyEvent(0,0, KeyEvent.ACTION_DOWN, keyCode,0, metaCode))
        inputConnection.sendKeyEvent(KeyEvent(0,0, KeyEvent.ACTION_UP, keyCode,0, metaCode))
    }

    fun write(char: Char) {
        inputConnection.commitText(char.toString(), 1)
    }

    fun currentPos(): Int {
        val extractedText = inputConnection.getExtractedText(ExtractedTextRequest(), 0)
        return extractedText.startOffset + extractedText.selectionStart
    }

    fun prevText() = inputConnection.getTextBeforeCursor(currentPos(), 0)

    fun backwardLengthForRemoval(): Int {
        val text = prevText().reversed()
        var hasSkippedWhitespace = false
        var length = 0

        for(char in text){
            when{
                !hasSkippedWhitespace && (char == '\n' || char == '/') -> {
                    return 1
                }

                !hasSkippedWhitespace && char == ' ' -> {
                    length++
                }

                char != ' ' && char != '\n' && char != '/' -> {
                    hasSkippedWhitespace = true
                    length++
                }

                else -> break
            }

        }

        return length
    }
}
