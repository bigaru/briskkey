package `in`.abaddon.briskkey

enum class ModifierState{
    UNSET ,
    SET_ONE_TIME,
    SET_PERMA
}

operator fun ModifierState.inc(): ModifierState {
    val newPos = (this.ordinal+1) % ModifierState.values().size
    return ModifierState.values()[newPos]
}

val ModifierState.isSet: Boolean
    get() = this == ModifierState.SET_ONE_TIME || this == ModifierState.SET_PERMA

val ModifierState.isOneTime: Boolean
    get() = this == ModifierState.SET_ONE_TIME

val ModifierState.isPerma: Boolean
    get() = this == ModifierState.SET_PERMA
