package `in`.abaddon.briskkey

import android.graphics.Rect
import android.inputmethodservice.InputMethodService
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import kotlinx.coroutines.*
import java.util.concurrent.atomic.AtomicBoolean


typealias TouchHandler = (View, MotionEvent) -> Boolean

class BriskInputMethodEditor: InputMethodService(), IMEHelper {
    override val inputConnection: InputConnection
        get() = currentInputConnection

    override val inputEditorInfo: EditorInfo
        get() = currentInputEditorInfo

    val scope = CoroutineScope(Dispatchers.Main + Job())
    lateinit var keyViewBoundaries: Rect

    lateinit var keyboardView: KeyboardView
    var keyboardState = KeyboardState(ModifierState.UNSET, ModifierState.UNSET, ModifierState.UNSET)

    val hasEnteredLongPress = AtomicBoolean(false)
    val LONG_PRESS_DELAY = ViewConfiguration.getLongPressTimeout().toLong()

    val LONG_DELAY = 400L
    val SHORT_DELAY = 200L
    val NO_OF_SLOW_REMOVAL = 3

    fun resetMods(){
        keyboardState.resetIfOneTime()
        updateLayout()
    }

    fun updateLayout(){
        keyboardView.updateBoard(keyboardState.getNeoLayout(), keyboardState)
    }

    fun handleClick(key: Key) {
        when(key) {
            is Sign -> {
                write(key.sign)
                resetMods()
            }
            is ENTER -> {
                performEnter()
                resetMods()
            }
            is BACKSPACE -> {
                sendKeyCode(KeyEvent.KEYCODE_DEL)
                resetMods()
            }
            is SPACE -> {
                write(Keys.SIGN_SPACE)
                resetMods()
            }
            is SHIFT, is M3, is M4, -> {
                handleModifierKey(key)
                updateLayout()
            }
            is Control -> {
                handleControlKey(key)
                resetMods()
            }
        }
    }

    fun handleModifierKey(key: Key) {
        when(key){
            is SHIFT -> keyboardState.modShift++
            is M3 -> keyboardState.modM3++
            is M4 -> keyboardState.modM4++
        }
    }

    fun handleControlKey(key: Control) {
        when(key.sign) {
            Keys.PAGE_UP -> sendKeyCode(KeyEvent.KEYCODE_PAGE_UP)
            Keys.BACKWARD_DEL -> sendKeyCode(KeyEvent.KEYCODE_DEL)
            Keys.ARROW_UP -> sendKeyCode(KeyEvent.KEYCODE_DPAD_UP)
            Keys.FORWARD_DEL -> sendKeyCode(KeyEvent.KEYCODE_FORWARD_DEL)
            Keys.PAGE_DOWN -> sendKeyCode(KeyEvent.KEYCODE_PAGE_DOWN)

            Keys.HOME -> sendKeyCode(KeyEvent.KEYCODE_MOVE_HOME)
            Keys.ARROW_LEFT -> sendKeyCode(KeyEvent.KEYCODE_DPAD_LEFT)
            Keys.ARROW_DOWN -> sendKeyCode(KeyEvent.KEYCODE_DPAD_DOWN)
            Keys.ARROW_RIGHT -> sendKeyCode(KeyEvent.KEYCODE_DPAD_RIGHT)
            Keys.END -> sendKeyCode(KeyEvent.KEYCODE_MOVE_END)

            Keys.CLEAR -> sendKeyCode(KeyEvent.KEYCODE_CLEAR)
            Keys.TAB -> sendKeyCode(KeyEvent.KEYCODE_TAB)
            Keys.INSERT -> sendKeyCode(KeyEvent.KEYCODE_INSERT)
            Keys.UNDO -> sendKeyCode(KeyEvent.KEYCODE_Z, KeyEvent.META_CTRL_ON)

            else -> throw AssertionError("Case not handled")
        }
    }

    suspend fun handleDuringPressing (key: Key) {
        hasEnteredLongPress.set(true)

        when(key) {
            is BACKSPACE -> {
                var counter = 1
                var currentPos = currentPos()

                while (hasEnteredLongPress.get() && currentPos > 0) {
                    currentInputConnection.deleteSurroundingText(backwardLengthForRemoval(),0)
                    keyboardView.performHapticFeedback()
                    resetMods()

                    val delayBetween = if(counter > NO_OF_SLOW_REMOVAL) SHORT_DELAY else LONG_DELAY
                    delay(delayBetween)

                    counter++
                    currentPos = currentPos()
                }
            }

            is SHIFT -> {
                keyboardState.toggleShift()
                updateLayout()
                keyboardView.performHapticFeedback()
            }
            is M3 -> {
                keyboardState.toggleM3()
                updateLayout()
                keyboardView.performHapticFeedback()
            }
            is M4 -> {
                keyboardState.toggleM4()
                updateLayout()
                keyboardView.performHapticFeedback()
            }
        }
    }

    fun handleLongClick(key: Key){
        when (key){
            is Sign -> {
                write(key.sign)
                resetMods()
            }
            is ENTER -> {
                performEnter()
                resetMods()
            }
            is SPACE -> {
                write(Keys.SIGN_SPACE)
                resetMods()
            }
            is Control -> {
                handleControlKey(key)
                resetMods()
            }
        }
    }

    private val touchHandler: TouchHandler = { v, event ->
        val key = v.tag as Key

        when(event.actionMasked){
            MotionEvent.ACTION_DOWN -> {
                keyboardView.performHapticFeedback()

                scope.launch {
                    delay(LONG_PRESS_DELAY)
                    scope.launch { handleDuringPressing(key) }
                }

                v.isPressed = true
                keyViewBoundaries = Rect(v.left, v.top, v.right, v.bottom)

                true
            }

            MotionEvent.ACTION_CANCEL -> {
                hasEnteredLongPress.set(false)
                scope.coroutineContext.cancelChildren()
                v.isPressed = false
                true
            }

            MotionEvent.ACTION_UP -> {
                if(!hasEnteredLongPress.get() && v.isPressed) {
                    handleClick(key)
                    v.performClick()
                }

                if(hasEnteredLongPress.get() && v.isPressed) {
                    handleLongClick(key)
                }

                hasEnteredLongPress.set(false)
                scope.coroutineContext.cancelChildren()
                v.isPressed = false

                true
            }

            MotionEvent.ACTION_MOVE -> {
                val xPoint = v.left + event.x.toInt()
                val yPoint = v.top + event.y.toInt()

                if(!keyViewBoundaries.contains(xPoint, yPoint)){
                    hasEnteredLongPress.set(false)
                    scope.coroutineContext.cancelChildren()
                    v.isPressed = false
                }

                true
            }

            else -> false
        }

    }

    override fun onCreate() {
        setTheme(R.style.AppTheme)
        super.onCreate()
    }

    override fun onCreateInputView(): View {
        val viewFactory =  ViewFactory(this, touchHandler)
        keyboardView = viewFactory.createKeyboardView()

        updateLayout()
        return keyboardView.root
    }

    override fun onStartInputView(info: EditorInfo?, restarting: Boolean) {
        keyboardState = KeyboardState(ModifierState.UNSET, ModifierState.UNSET, ModifierState.UNSET)
        updateLayout()
    }

    override fun onDestroy() {
        scope.coroutineContext.cancelChildren()
        super.onDestroy()
    }
}
