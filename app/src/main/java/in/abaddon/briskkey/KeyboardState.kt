package `in`.abaddon.briskkey

data class KeyboardState(var modShift: ModifierState, var modM3: ModifierState, var modM4: ModifierState){
    fun resetIfOneTime() {
        if(modShift == ModifierState.SET_ONE_TIME) { modShift = ModifierState.UNSET }
        if(modM3 == ModifierState.SET_ONE_TIME) { modM3 = ModifierState.UNSET }
        if(modM4 == ModifierState.SET_ONE_TIME) { modM4 = ModifierState.UNSET }
    }

    fun getNeoLayout(): Board = when {
        modM3.isSet && modM4.isSet -> Keys.layer6
        modShift.isSet && modM3.isSet -> Keys.layer5
        modM4.isSet -> Keys.layer4
        modM3.isSet -> Keys.layer3
        modShift.isSet -> Keys.layer2
        else -> Keys.layer1
    }

    fun toggleShift(){
        if(modShift.isSet) {
            modShift = ModifierState.UNSET
        } else {
            modShift = ModifierState.SET_PERMA
        }
    }

    fun toggleM3(){
        if(modM3.isSet) {
            modM3 = ModifierState.UNSET
        } else {
            modM3 = ModifierState.SET_PERMA
        }
    }

    fun toggleM4(){
        if(modM4.isSet) {
            modM4 = ModifierState.UNSET
        } else {
            modM4 = ModifierState.SET_PERMA
        }
    }
}
