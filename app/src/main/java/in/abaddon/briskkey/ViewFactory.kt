package `in`.abaddon.briskkey

import android.content.Context
import android.view.View
import android.view.ViewGroup.LayoutParams
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Button
import android.widget.LinearLayout

class ViewFactory(override val ctx: Context, val touchHandler: TouchHandler): ViewHelpers {
    fun createKeyboardView(): KeyboardView {
        val rowVHs = (0..4).map { createRowVH() }
        val root = layout(
            LinearLayout.VERTICAL,
            MATCH_PARENT,
            WRAP_CONTENT,
            rowVHs.map { it.row }
        )

        return KeyboardView(root, rowVHs)
    }

    fun createRowVH(): RowVH {
        val btns = (0..10).map { btn() }
        val row = layout(LinearLayout.HORIZONTAL, MATCH_PARENT, WRAP_CONTENT, btns, bgColor = R.color.bg, padding = 2)
        return RowVH(row, btns)
    }

    fun btn(): Button {
        return Button(ctx).apply {
            text = ""
            background = drawable(R.drawable.any_key)

            setTextColor(color(android.R.color.white))
            layoutParams = weightedParams(1f)

            minWidth = 0
            minHeight = 0

            minimumWidth = 0
            minimumHeight = 0

            textSize = 10.sp
            setPadding(8.dp, 6.dp, 8.dp, 6.dp)

            isAllCaps = false
            setTypeface(font(R.font.dejavu_mono))

            setOnTouchListener(touchHandler)
        }
    }

    fun layout(
        orient: Int,
        width: Int,
        height: Int,
        children: List<View>,
        bgColor: Int = android.R.color.transparent,
        padding: Int = 0
    ): LinearLayout {
        return LinearLayout(ctx).apply {
            orientation = orient
            layoutParams = LayoutParams(width, height)
            setBackgroundColor(color(bgColor))
            setPadding(padding.dp, padding.dp, padding.dp, padding.dp)

            children.forEach { c -> addView(c) }
        }
    }
}
