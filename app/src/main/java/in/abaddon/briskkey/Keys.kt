package `in`.abaddon.briskkey

sealed class Key
data class Sign(val sign: Char): Key()
data class Control(val sign: Char): Key()

object SHIFT: Key()
object M3: Key()
object M4: Key()
object ENTER: Key()
object BACKSPACE: Key()
object SPACE: Key()

interface NeoHelper {
    fun label(key: Key) = when(key){
        is Sign -> key.sign.toString()
        is Control -> key.sign.toString()
        is SHIFT -> "⇧"
        is M3 -> "M3"
        is M4 -> "M4"
        is ENTER -> "↵"
        is BACKSPACE -> "⌫"
        is SPACE -> " "
    }

    fun weight(key: Key, kState: KeyboardState) = when{
        key is SPACE -> 4f
        // TODO handle differently
        key is Sign && key.sign == '0' && kState.modM4.isSet -> 4f
        else -> 1f
    }

    fun bg(key: Key, kState: KeyboardState) = when {
        key is SHIFT && kState.modShift.isOneTime -> R.drawable.onetime_active_key
        key is SHIFT && kState.modShift.isPerma -> R.drawable.perma_active_key

        key is M3 && kState.modM3.isOneTime -> R.drawable.onetime_active_key
        key is M3 && kState.modM3.isPerma -> R.drawable.perma_active_key

        key is M4 && kState.modM4.isOneTime -> R.drawable.onetime_active_key
        key is M4 && kState.modM4.isPerma -> R.drawable.perma_active_key

        key is SPACE -> R.drawable.space_key
        else -> R.drawable.any_key
    }
}

typealias Board = List<List<Key>>

object Keys {
    val SIGN_SPACE = ' '
    val LINE_ENDING = '\n'

    val PAGE_UP = '⇞'
    val PAGE_DOWN = '⇟'
    val BACKWARD_DEL = '⌫'
    val FORWARD_DEL = '⌦'
    val ARROW_UP = '⇡'
    val ARROW_DOWN = '⇣'
    val ARROW_LEFT = '⇠'
    val ARROW_RIGHT = '⇢'
    val HOME = '⇱'
    val END = '⇲'
    val CLEAR = '⌧'
    val TAB = '⇥'
    val INSERT = 9088.toChar()
    val UNDO = '↶'


    val layer1: Board = listOf(
        listOf('1',  '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0',  '-').map { Sign(it) },
        listOf('x',  'v',  'l',  'c',  'w',  'k',  'h',  'g',  'f',  'q',  'ß').map { Sign(it) },
        listOf('u',  'i',  'a',  'e',  'o',  's',  'n',  'r',  't',  'd',  'y').map { Sign(it) },
        listOf('ü',  'ö',  'ä',  'p',  'z',  'b',  'm',  ',',  '.',  'j').map { Sign(it) } + listOf(BACKSPACE),
        listOf(SHIFT, M3, SPACE, M4, ENTER)
    )

    val layer2: Board = listOf(
        listOf('°',  '§',  'ℓ',  '»',  '«',  '$',  '€',  '„',  '“',  '”',  '—').map{Sign(it)},
        listOf('X',  'V',  'L',  'C',  'W',  'K',  'H',  'G',  'F',  'Q',  'ẞ').map{Sign(it)},
        listOf('U',  'I',  'A',  'E',  'O',  'S',  'N',  'R',  'T',  'D',  'Y').map{Sign(it)},
        listOf('Ü',  'Ö',  'Ä',  'P',  'Z',  'B',  'M',  '–',  '•',  'J').map{Sign(it)} + listOf(BACKSPACE),
        listOf(SHIFT, M3, SPACE, M4, ENTER)
    )

    val layer3: Board = listOf(
        listOf('¹',  '²',  '³',  '›',  '‹',  '¢',  '¥',  '‚',  '‘',  '’',  '-').map{Sign(it)},
        listOf('…',  '_',  '[',  ']',  '^',  '!',  '<',  '>',  '=',  '&',  'ſ').map{Sign(it)},
        listOf('\\',  '/',  '{',  '}',  '*', '?',  '(',  ')',  '-',  ':',  '@').map{Sign(it)},
        listOf('#',  '$',  '|', '~',  '`',  '+',  '%',  '"',  '\'',  ';').map{Sign(it)} + listOf(BACKSPACE),
        listOf(SHIFT, M3, SPACE, M4, ENTER)
    )

    val layer4: Board = listOf(
        listOf('ª',  'º',  '№',  '·',  '£',  '¤').map{Sign(it)} + listOf(Control(TAB)) + listOf('/',  '*',  '-').map{Sign(it)},
        listOf(PAGE_UP, BACKWARD_DEL, ARROW_UP, FORWARD_DEL, PAGE_DOWN).map{Control(it)} + listOf('¡', '7', '8', '9', '+', '−').map{Sign(it)},
        listOf(HOME, ARROW_LEFT, ARROW_DOWN, ARROW_RIGHT, END).map{Control(it)} + listOf('¿', '4', '5', '6', ',', '.').map{Sign(it)},
        listOf(CLEAR, TAB, INSERT).map{Control(it)} + listOf(ENTER, Control(UNDO)) + listOf(':', '1', '2', '3', ';').map{Sign(it)} + listOf(BACKSPACE),
        listOf(SHIFT, M3, Sign('0'), M4, ENTER)
    )

    val layer5: Board = listOf(
        listOf('₁',  '₂',  '₃',  '♀',  '♂',  '⚥',  'ϰ',  '⟨',  '⟩',  '₀',  '‑').map{Sign(it)},
        listOf('ξ',  'λ',  'χ',  'ω',  'κ',  'ψ',  'γ',  'φ',  'ϕ',  'ς').map{Sign(it)},
        listOf('ι',  'α',  'ε',  'ο', 'σ',  'ν',  'ρ',  'τ',  'δ',  'υ').map{Sign(it)},
        listOf('ϵ',  'η', 'π',  'ζ',  'β',  'μ', 'ϱ',  'ϑ',  'θ').map{Sign(it)} + listOf(BACKSPACE),
        listOf(SHIFT, M3, SPACE, M4, ENTER)
    )

    val layer6: Board = listOf(
        listOf('¬',  '∨',  '∧',  '⊥',  '∡',  '∥',  '→',  '∞',  '∝',  '∅', '╌').map{Sign(it)},
        listOf('Ξ',  '√',  'Λ',  'ℂ',  'Ω',  '×',  'Ψ',  'Γ',  'Φ',  'ℚ',  '∘').map{Sign(it)},
        listOf('⊂',  '∫',  '∀',  '∃',  '∈',  'Σ',  'ℕ',  'ℝ',  '∂',  'Δ',  '∇').map{Sign(it)},
        listOf('∪',  '∩',  'ℵ',  'Π',  'ℤ',  '⇐',  '⇔',  '⇒',  '↦',  'Θ').map{Sign(it)} + listOf(BACKSPACE),
        listOf(SHIFT, M3, SPACE, M4, ENTER)
    )
}
