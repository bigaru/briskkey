package `in`.abaddon.briskkey

import android.content.Context
import android.widget.Button
import android.widget.LinearLayout

data class RowVH(val row: LinearLayout, val allViews: List<Button>): ViewHelpers, NeoHelper {
    override val ctx: Context = row.context

    fun onBind(keyList: List<Key>, keyboardState: KeyboardState){
        keyList.forEachIndexed { idx, key ->
            allViews[idx].apply {
                text = label(key)
                layoutParams = weightedParams(weight(key, keyboardState))
                background = drawable(bg(key, keyboardState))
                tag = key
            }
        }

        val diff = row.childCount - keyList.size
        if(diff > 0) {
            val startPos = keyList.size
            val endPos = row.childCount - 1
            (startPos..endPos).forEach { row.removeView(allViews[it]) }
        }

        if(diff < 0) {
            val startPos = row.childCount
            val endPos = row.childCount - 1 + (-diff)
            (startPos..endPos).forEach { row.addView(allViews[it]) }
        }
    }
}
