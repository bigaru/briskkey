package `in`.abaddon.briskkey

import android.content.Context
import android.util.TypedValue
import android.widget.LinearLayout
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.FontRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat

interface ViewHelpers {
    val ctx: Context

    val Int.dp: Int
        get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), ctx.resources.displayMetrics).toInt()

    val Int.sp: Float
        get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, this.toFloat(), ctx.resources.displayMetrics)

    fun weightedParams(weight: Float): LinearLayout.LayoutParams {
        return LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, weight)
    }

    fun drawable(@DrawableRes res: Int) = ContextCompat.getDrawable(ctx, res)
    fun color(@ColorRes res: Int) = ContextCompat.getColor(ctx, res)
    fun font(@FontRes res: Int) = ResourcesCompat.getFont(ctx, res)
}
