package `in`.abaddon.briskkey

import android.view.HapticFeedbackConstants
import android.widget.LinearLayout

class KeyboardView (val root: LinearLayout, val rows: List<RowVH>){
    fun performHapticFeedback() =
        root.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP)

    fun updateBoard(board: Board, keyboardState: KeyboardState){
        board.forEachIndexed { idx, keyList ->
            rows[idx].onBind(keyList, keyboardState)
        }
    }
}
